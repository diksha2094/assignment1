import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.omg.CORBA.SystemException;

public class YellingTest {
	Yelling y=new Yelling();
	@Before
	public void setUp() throws Exception {
				
	}

	@After
	public void tearDown() throws Exception {
	}
  // R1-Peter is yelling
	@Test
	public void screamtest() {
		String output=y.scream("Peter");
		assertEquals("Peter is yelling",output);
	}
	// R2- Nobody is yelling
	@Test
	public void screamTest1() {
		
		String output=y.scream("null");
		assertEquals("Nobody is yelling",output);
	}
	// R3- PETER IS YELLING
		@Test
		public void screamTest2() {
			
			String output=y.scream("PETER");
			assertEquals("PETER IS YELLING",output);
		}
    // R4- Peter and Kadeem are yelling
	// R4- Albert and Pritesh are yelling
		@Test
		public void screamTest3() {
			
			String output=y.scream("joined");
			String output1=y.scream("joined1");
			assertEquals("Peter and Kadeem are yelling",output);
			assertEquals("Albert and Pritesh are yelling",output1);
						
		
		}
		
		// R5- More than 2 people are yelling
		
			@Test
			public void screamTest4() {
				String[] name=new String[] {"Peter","Kadeem","Albert"};
				String output=y.join(name);
				String output1=y.scream("output");
				assertEquals("Peter,Kadeem and Albert are yelling",output1);
			}

			// R6- Shouting at a lot of people
			
				@Test
				public void screamTest5() {
					String[] name=new String[] {"Peter","EMAD"};
					String output=y.Sois(name);
					String output1=y.scream("output");
					assertEquals("Peter is yelling. SO IS EMAD!",output1);
				}
			
			
}
